using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    [Range(1, 10)] public float velocidad; //Velocidad del jugador
    Rigidbody2D rb2d;
    SpriteRenderer spRd;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        spRd = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float movimientoH = Input.GetAxisRaw("Horizontal");
        rb2d.velocity = new Vector2(movimientoH * velocidad, rb2d.velocity.y);
        float movimientoV = Input.GetAxisRaw("Vertical");
        rb2d.velocity = new Vector2(rb2d.velocity.x, movimientoV * velocidad);
        if (movimientoH <0)
        {
            spRd.flipX = false;
        }
        else if (movimientoH > 0)
        {
            spRd.flipX = true;
        }
    }
}
